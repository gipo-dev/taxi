<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

$factory->define(\App\Post::class, function (Faker $faker) {
    $title = $faker->sentence($faker->numberBetween(4, 8));

    $related = [];
    for ($i = 0; $i < 5; $i++)
        $related[] = $faker->numberBetween(0, 30);

    $body = '';
    foreach ($faker->paragraphs($faker->numberBetween(10, 30)) as $paragraph)
        $body .= '<p>' . $paragraph . '</p>';

    $image = $faker->image();
    $imageFile = new File($image);

    return [
        'title' => $title,
        'body' => $body,
        'image' => '/storage/' . Storage::disk('public')->putFile('img/temp', $imageFile),
        'slug' => Str::slug($title),
        'related' => $related,
        'meta_title' => 'Meta ' . $title,
        'meta_description' => $faker->sentence($faker->numberBetween(3, 5)),
    ];
});
