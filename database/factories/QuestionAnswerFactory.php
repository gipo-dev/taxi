<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\QuestionAnswer::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence($faker->numberBetween(3, 10)) . '?',
        'answer' => $faker->sentence($faker->numberBetween(10, 200)),
    ];
});
