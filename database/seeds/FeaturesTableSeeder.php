<?php

use App\Feature;
use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $features = [
            'Помощь в подключении<br>водителям-иностранцам',
            'Бесплатная помощь<br>в прохождении фотоконтроля',
            'Бесплатная помощь в<br>получении статуса “Корона”',
            'Заправка<br>с таксометра',
            'Возможность<br>покупки смен',
            'Моментальные<br>выплаты',
            'Прозрачная<br>комиссия от 3%',
            'Свободный график<br>работы',
        ];
        foreach ($features as $i => $feature) {
            Feature::create([
                'icon' => 'icon_' . ($i + 1),
                'text' => $feature,
            ]);
        }
    }
}
