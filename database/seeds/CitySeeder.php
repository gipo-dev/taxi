<?php

use App\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            ['Санкт-Петербург', 'Санкт-Петербурге', 'Санкт-Петербурге и Ленинградской области'],
            ['Москва', 'Москве', 'Москве и Московской области'],
            ['Екатеринбург', 'Екатеринбурге', 'Екатеринбурге и Екатеринбуржской области'],
        ];
        foreach ($cities as $city)
            City::create([
                'name' => $city[0],
                'name_gen' => $city[1],
                'name_long' => $city[2],
                'slug' => Str::slug($city[0]),
                'image' => '',
            ]);
    }
}
