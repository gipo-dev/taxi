<?php

namespace Tests\Feature;

use App\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CityTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $cities = City::get();
        foreach ($cities as $city) {
            $response = $this->get(route('city', $city));

            if ($city->slug == 'sankt-peterburg')
                $response->assertRedirect(route('index'));
            else {
                $response->assertStatus(200);
                $response->assertSee($city->name_gen);
            }
        }
    }
}
