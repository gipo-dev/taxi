<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class SeoMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Str::startsWith($request->path(), 'dashboard')) {
            $config = config('seo.' . $request->route()->getName()) ?? config('seo.default');
            foreach ($config as $key => $value)
                \SEO::{'set' . ucfirst($key)}($value);
        }
        return $next($request);
    }
}
