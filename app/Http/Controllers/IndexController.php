<?php

namespace App\Http\Controllers;

use App\Feature;
use App\QuestionAnswer;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $features = Feature::get();
        $questions = QuestionAnswer::get();

        return view('index', compact('features', 'questions'));
    }
}
