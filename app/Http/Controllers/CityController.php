<?php

namespace App\Http\Controllers;

use App\City;
use App\Feature;
use App\QuestionAnswer;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * @param Request $request
     * @param City $city
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, City $city)
    {
        if ($city->slug == 'sankt-peterburg')
            return redirect(route('index'));

        \SEO::setTitle(config('seo.city.title') . $city->name_gen);
        \SEO::setDescription(str_replace(':city', $city->name_long, config('seo.city.description')));

        $features = Feature::get();
        $questions = QuestionAnswer::get();
        return view('index', compact('city', 'features', 'questions'));
    }

    public function list(Request $request)
    {
        $cities = City::get();
        return view('cities', compact('cities'));
    }
}
