<?php

namespace App\Http\Controllers;


use App\ConnectionRequest;
use App\Events\Request\ConnectionRequestSent;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    /**
     * @param Request $request
     * @return string
     */
    public function send(Request $request)
    {
        $connectionRequest = ConnectionRequest::create($request->except(['_token',]));

        event(new ConnectionRequestSent($connectionRequest));
        dd($connectionRequest);

        return response()->json('ok');
    }
}
