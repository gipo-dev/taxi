<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostCategory;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @param Request $request
     * @param Post $post
     */
    public function index(Request $request, Post $post)
    {
        \SEO::setTitle($post->meta_title);
        \SEO::setDescription($post->meta_description);
        return view('post.show', compact('post'));
    }

    /**
     * @param Request $request
     */
    public function list(Request $request)
    {
        $posts = Post::latest()->paginate(10);
        return view('post.list', compact('posts'));
    }

    /**
     * @param Request $request
     * @param PostCategory $category
     */
    public function category(Request $request, PostCategory $category)
    {
        return view('post.category', compact('category'));
    }
}
