<?php

namespace App\Http\Controllers;

use App\Cars\CarModel;
use App\Cars\Tariff;
use Illuminate\Http\Request;

class TariffController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tariffs = Tariff::with('cars', 'cars.model', 'cars.model.brand')
            ->orderBy('created_at', 'asc')
            ->get();
        return view('tariffs', compact('tariffs'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function models(Request $request)
    {
        $options = CarModel::where('car_brand_id', $request->brand_id)
            ->orderBy('name')
            ->get()
            ->pluck('name', 'id')
            ->toArray();
        $options = [-1 => 'Выберите модель',] + $options;
        return view('partials.options', compact('options'));
    }

    public function car(Request $request)
    {
        $car = CarModel::find($request->model_id);
        $tariffs = $car->tariffs;
        return view('partials.car_tariff_result', ['car' => $car->full_name, 'tariffs' => $tariffs]);
    }
}
