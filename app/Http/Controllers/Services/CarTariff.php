<?php

namespace App\Http\Controllers\Services;

use App\Cars\CarBrand;
use App\Cars\CarModel;
use App\Cars\Tariff;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CarTariff extends Controller
{
    public function handle()
    {
        $tariffs = ['Эконом', 'Business', 'Elite', 'Premier', 'Детский', 'Комфорт', 'Комфорт_плюс', 'Минивэн'];
        $cars = [];
        foreach ($tariffs as $tariff) {
            dd(123);
            $data = file_get_contents(storage_path('cars/' . $tariff . '.txt'));
            preg_match_all('$<p[^>]*>(.*?)</p>$', $data, $lines);
            foreach ($lines[1] as $i => $line) {
                if ($i % 2 !== 0)
                    continue;
                $car = $this->getCarModel($line);
                $car['min'] = str_replace('от ', '', $lines[1][$i + 1]);
//                $cars[$car['label']][$car['model']][$tariff] = $car['min'];
                $car_label = CarBrand::firstOrCreate([
                    'name' => $car['label'],
                ]);
                $car_model = CarModel::firstOrCreate([
                    'name' => $car['model'],
                    'car_brand_id' => $car_label->id,
                ]);
                $t = Tariff::where('name', $tariff)->first();
                \App\Cars\CarTariff::create([
                    'car_id' => $car_model->id,
                    'tariff_id' => $t->id,
                    'min_year' => $car['min'],
                ]);
            }
        }
        dd($cars);
    }

    private function getCarModel(string $line)
    {
        $exp = explode(' ', $line);
        $data = [];
        $data['label'] = $exp[0];
        unset($exp[0]);
        $data['model'] = implode(' ', $exp);
        return $data;
    }
}
