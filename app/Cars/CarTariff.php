<?php

namespace App\Cars;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CarTariff
 * @package App\Cars
 *
 * @property int car_id
 * @property int tariff_id
 * @property int min_year
 */
class CarTariff extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function model()
    {
        return $this->hasOne(CarModel::class, 'id', 'car_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tariff()
    {
        return $this->belongsTo(Tariff::class, 'tariff_id');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->tariff->name;
    }

    /**
     * @return string
     */
    public function getTariffRequirementsAttribute()
    {
        if (is_numeric($this->min_year))
            return 'от ' . $this->min_year . ' г.в.';
        return $this->min_year;
    }
}
