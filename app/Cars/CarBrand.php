<?php

namespace App\Cars;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CarBrand
 * @package App\Cars
 *
 * @property string name
 */
class CarBrand extends Model
{
    public $timestamps = false;

    protected $guarded = [];
}
