<?php

namespace App\Cars;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CarModel
 * @package App\Cars
 *
 * @property string name
 * @property int car_brand_id
 */
class CarModel extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    public function getFullNameAttribute()
    {
        return $this->brand->name . ' ' . $this->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(CarBrand::class, 'car_brand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tariffs()
    {
        return $this->hasMany(CarTariff::class, 'car_id');
    }
}
