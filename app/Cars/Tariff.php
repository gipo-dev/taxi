<?php

namespace App\Cars;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tariff
 * @package App\Cars
 *
 * @property string name
 * @property string comment
 */
class Tariff extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['name', 'comment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function cars()
    {
        return $this->hasMany(CarTariff::class);
//        return $this->hasManyThrough(CarModel::class, CarTariff::class, 'tariff_id', 'id', 'id')->where('tariff_id', $this->id)->dd();
    }
}
