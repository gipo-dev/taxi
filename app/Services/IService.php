<?php


namespace App\Services;


interface IService
{
    /**
     * @param string $message
     * @return bool
     */
    public function send($message);
}
