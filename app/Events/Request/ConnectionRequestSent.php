<?php

namespace App\Events\Request;

use App\ConnectionRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ConnectionRequestSent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var ConnectionRequest
     */
    public $connectionRequest;

    /**
     * Create a new event instance.
     *
     * @param ConnectionRequest $connectionRequest
     */
    public function __construct(ConnectionRequest $connectionRequest)
    {
        $this->connectionRequest = $connectionRequest;
    }
}
