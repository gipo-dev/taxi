<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Orchid\Access\UserAccess;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;
use Ramsey\Collection\Collection;

/**
 * Class Post
 * @package App
 * @property string $title
 * @property string $body
 * @property string $image
 * @property string $slug
 * @property array $related
 * @property string $meta_title
 * @property string $meta_description
 */
class Post extends Model
{

    use AsSource, UserAccess, AsSource, Filterable, Chartable;

    /**
     * @var string[]
     */
    protected $attributes = ['related' => '[]',];

    /**
     * @var string[]
     */
    protected $casts = [
        'title' => 'string',
        'body' => 'string',
        'image' => 'string',
        'slug' => 'string',
        'meta_title' => 'string',
        'meta_description' => 'string',
        'related' => 'array',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'body',
        'image',
        'slug',
        'related',
        'meta_title',
        'meta_description',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'title',
        'body',
        'slug',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'title',
        'body',
        'slug',
        'created_at',
    ];

    /**
     * @return string
     */
    public function getShortBodyAttribute()
    {
        $text = $this->attributes['body'];
        $text = str_replace(['</p><p>', '</p>', '<p>'], ['', '', PHP_EOL], $text);
        $text = strip_tags($text);
        return mb_substr($text, 0, 400);
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        if (!Str::startsWith(request()->path(), 'dashboard'))
            return 'slug';
        return 'id';
    }

    /**
     * @return Collection<Post>
     */
    public function related()
    {
        return Post::find($this->related);
    }
}
