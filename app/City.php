<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Orchid\Access\UserAccess;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * Class City
 * @package App
 *
 * @property string name
 * @property string name_gen
 * @property string name_long
 * @property string slug
 * @property string image
 */

class City extends Model
{
    use AsSource, UserAccess, AsSource, Filterable, Chartable;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'name_gen' => 'string',
        'name_long' => 'string',
        'slug' => 'string',
        'image' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'name_gen',
        'name_long',
        'slug',
        'image',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'name',
        'name_gen',
        'name_long',
        'slug',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'name',
        'name_gen',
        'name_long',
        'slug',
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        if (!Str::startsWith(request()->path(), 'dashboard'))
            return 'slug';
        return 'id';
    }
}
