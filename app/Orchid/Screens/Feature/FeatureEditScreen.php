<?php

namespace App\Orchid\Screens\Feature;

use App\Feature;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class FeatureEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Создание преимущества';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * @var Feature
     */
    protected $feature;

    /**
     * @var bool
     */
    protected $edit = true;

    /**
     * Query data.
     *
     * @param Feature $feature
     * @return array
     */
    public function query(Feature $feature): array
    {
        $this->feature = $feature;
        $this->edit = $feature->exists;
        if ($this->edit)
            $this->name = 'Редактирование преимущества';

        return [
            'feature' => $feature,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [

            Button::make('Сохранить')
                ->icon('icon-check')
                ->class('btn btn-success')
                ->method('createOrUpdate'),

            Button::make('Удалить')
                ->icon('icon-trash')
                ->class('btn btn-danger ml-2')
                ->confirm('Удалить?')
                ->method('remove')
                ->canSee($this->edit),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Cropper::make('feature.icon')
                    ->width(200)
                    ->height(200)
                    ->title('Изображение')
                    ->required(),

                TextArea::make('feature.text')
                    ->title('Текст')
                    ->rows(5)
                    ->class('form-control w-100')
                    ->required(),
            ]),
        ];
    }

    /**
     * @param Feature $post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Feature $feature, Request $request)
    {
        $feature->fill($request->feature)->save();

        Alert::success('Обновлено');

        return redirect()->route('platform.features');
    }

    /**
     * @param Feature $feature
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Feature $feature, Request $request)
    {
        $feature->delete();

        Alert::success('Удалено');

        return redirect()->route('platform.features');
    }
}
