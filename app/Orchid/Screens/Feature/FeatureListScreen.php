<?php

namespace App\Orchid\Screens\Feature;

use App\Feature;
use App\Orchid\Layouts\Feature\FeatureListLayout;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class FeatureListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Блоки преимущества';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'features' => Feature::filters()
                ->orderBy('id', 'desc')
                ->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('icon-plus')
                ->class('btn btn-primary')
                ->route('platform.features.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            FeatureListLayout::class,
        ];
    }
}
