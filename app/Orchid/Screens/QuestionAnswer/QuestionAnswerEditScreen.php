<?php

namespace App\Orchid\Screens\QuestionAnswer;

use App\QuestionAnswer;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class QuestionAnswerEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Новый вопрос-ответ';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * @var QuestionAnswer
     */
    protected $questionAnswer;

    /**
     * @var bool
     */
    protected $edit = true;

    /**
     * Query data.
     *
     * @param QuestionAnswer $question
     * @return array
     */
    public function query(QuestionAnswer $question): array
    {
        $this->questionAnswer = $question;
        $this->edit = $question->exists;
        if ($this->edit)
            $this->name = 'Редактирование вопроса-ответа';

        return [
            'question' => $this->questionAnswer,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [

            Button::make('Сохранить')
                ->icon('icon-check')
                ->class('btn btn-success')
                ->method('createOrUpdate'),

            Button::make('Удалить')
                ->icon('icon-trash')
                ->class('btn btn-danger ml-2')
                ->confirm('Удалить?')
                ->method('remove')
                ->canSee($this->edit),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                TextArea::make('question.question')
                    ->title('Вопрос')
                    ->rows(10)
                    ->class('w-100')
                    ->required(),

                TextArea::make('question.answer')
                    ->title('Ответ')
                    ->rows(10)
                    ->class('w-100')
                    ->required(),
            ]),
        ];
    }

    /**
     * @param QuestionAnswer $questionAnswer
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(QuestionAnswer $questionAnswer, Request $request)
    {
        $questionAnswer->fill($request->question)->save();

        Alert::success('Обновлено');

        return redirect()->route('platform.questions');
    }

    /**
     * @param QuestionAnswer $questionAnswer
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(QuestionAnswer $questionAnswer, Request $request)
    {
        $questionAnswer->delete();

        Alert::success('Удалено');

        return redirect()->route('platform.questions');
    }
}
