<?php

namespace App\Orchid\Screens\QuestionAnswer;

use App\Orchid\Layouts\QuestionAnswer\QuestionAnswerListLayout;
use App\QuestionAnswer;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class QuestionAnswerListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Вопросы-ответы';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'questions' => QuestionAnswer::filters()->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [

            Link::make('Добавить')
                ->icon('icon-plus')
                ->class('btn btn-primary')
                ->route('platform.questions.edit'),

        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            QuestionAnswerListLayout::class,
        ];
    }
}
