<?php

namespace App\Orchid\Screens\City;

use App\City;
use App\Orchid\Layouts\City\CityListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CityListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Подключаемые города';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'cities' => City::filters()->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('icon-plus')
                ->class('btn btn-primary')
                ->route('platform.cities.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            CityListLayout::class,
        ];
    }
}
