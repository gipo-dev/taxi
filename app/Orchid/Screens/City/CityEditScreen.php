<?php

namespace App\Orchid\Screens\City;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class CityEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Добавление города';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * @var City
     */
    protected $city;

    /**
     * @var bool
     */
    protected $edit = true;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(City $city): array
    {
        $this->city = $city;
        $this->edit = $city->exists;
        if ($this->edit)
            $this->name = 'Редактирование города';

        return [
            'city' => $city,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [

            Button::make('Сохранить')
                ->icon('icon-check')
                ->class('btn btn-success')
                ->method('createOrUpdate'),

            Button::make('Удалить')
                ->icon('icon-trash')
                ->class('btn btn-danger ml-2')
                ->confirm('Удалить?')
                ->method('remove')
                ->canSee($this->edit),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Cropper::make('city.image')
                    ->width(640)
                    ->height(360)
                    ->title('Изображение')
                    ->required(),

                Input::make('city.name')
                    ->title('Название')
                    ->required(),

                Input::make('city.name_gen')
                    ->title('Название род.')
                    ->required(),

                Input::make('city.name_long')
                    ->title('Полное название')
                    ->required(),
            ]),
        ];
    }


    /**
     * @param City $city
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(City $city, Request $request)
    {
        $data = $request->city;
        if (!$city->exists)
            $data['slug'] = Str::slug($data['name']);
        $city->fill($data)->save();

        Alert::success('Обновлено');

        return redirect()->route('platform.cities');
    }

    /**
     * @param City $city
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(City $city, Request $request)
    {
        $city->delete();

        Alert::success('Удалено');

        return redirect()->route('platform.cities');
    }
}
