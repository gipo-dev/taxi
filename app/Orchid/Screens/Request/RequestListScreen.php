<?php

namespace App\Orchid\Screens\Request;

use App\ConnectionRequest;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Field;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;

class RequestListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Запросы';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'requests' => ConnectionRequest::filters()->latest()->paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::table('requests', [
                TD::set('id', 'ID')
                    ->align('center')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function ($request) {
                        return Link::make('Подробнее')
                            ->route('platform.requests.view', $request)
                            ->icon('icon-eye')
                            ->class('btn btn-primary p-2');
                    }),
                TD::set('name', 'Имя')
                    ->sort()
                    ->filter(TD::FILTER_TEXT),
                TD::set('phone', 'Телефон')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function ($request) {
                        return "<a href='tel:{$request->phone}'>{$request->phone}</a>";
                    }),
                TD::set('source', 'Источник')
                    ->sort()
                    ->filter(TD::FILTER_TEXT),
                TD::set('ip', 'IP')
                    ->sort()
                    ->filter(TD::FILTER_TEXT),
            ]),
        ];
    }
}
