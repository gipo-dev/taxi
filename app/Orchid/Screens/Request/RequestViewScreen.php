<?php

namespace App\Orchid\Screens\Request;

use App\ConnectionRequest;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class RequestViewScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Запрос';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * @var ConnectionRequest
     */
    protected $item;

    /**
     * Query data.
     *
     * @param Request $request
     * @param ConnectionRequest $cr
     * @return array
     */
    public function query(Request $request, ConnectionRequest $cr): array
    {
        $cr = ConnectionRequest::find($request->cr);
        $this->item = $cr;

        return [
            'item' => $cr,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Удалить')
                ->icon('icon-trash')
                ->class('btn btn-danger ml-2')
                ->confirm('Удалить?')
                ->method('remove'),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([

                Input::make('item.name')
                    ->title('Имя'),

                Input::make('item.phone')
                    ->title('Телефон'),

                Input::make('item.source')
                    ->title('Источник'),

                Input::make('item.ip')
                    ->title('ip'),
            ]),
        ];
    }

    public function remove(ConnectionRequest $cr, Request $request)
    {
        $cr->delete();

        Alert::success('Удалено');

        return redirect()->route('platform.requests');
    }
}
