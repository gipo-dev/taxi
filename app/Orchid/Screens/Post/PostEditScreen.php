<?php

namespace App\Orchid\Screens\Post;

use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class PostEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Создание статьи';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * @var Post
     */
    protected $post;

    /**
     * @var bool
     */
    protected $edit = true;

    /**
     * Query data.
     *
     * @param Post $post
     * @param Request $request
     * @return array
     */
    public function query(Post $post, Request $request): array
    {
        $this->post = $post;
        $this->edit = $post->exists;
        if ($this->edit)
            $this->name = 'Редактирование статьи';

        return [
            'post' => $post,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->icon('icon-check')
                ->class('btn btn-success')
                ->method('createOrUpdate'),

            Button::make('Удалить')
                ->icon('icon-trash')
                ->class('btn btn-danger ml-2')
                ->confirm('Удалить?')
                ->method('remove')
                ->canSee($this->edit),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Cropper::make('post.image')
                    ->width(1920)
                    ->height(1080)
                    ->title('Изображение')
                    ->required(),

                TextArea::make('post.title')
                    ->title('Заголовок статьи')
                    ->max(255)
                    ->rows(3)
                    ->required(),

                Quill::make('post.body')
                    ->title('Тело поста')
                    ->required(),

                Input::make('post.meta_title')
                    ->title('Мета-заголовок')
                    ->max(255)
                    ->required(),

                TextArea::make('post.meta_description')
                    ->title('Мета-описание')
                    ->max(255)
                    ->rows(5)
                    ->required(),

                Input::make('post.slug')
                    ->title('Ссылка')
                    ->help('оставьте поле пустым для автоматической генерации')
                    ->max(255),

                DateTimer::make('post.created_at')
                    ->title('Дата публикации')
                    ->value($this->post->created_at ?? Carbon::now())
                    ->required(),

                Relation::make('post.related.')
                    ->fromModel(Post::class, 'title')
                    ->multiple()
                    ->title('Рекомендованные статьи'),

            ]),
        ];
    }

    /**
     * @param Post $post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Post $post, Request $request)
    {
        $data = $request->post;
        if (!$data['slug'])
            $data['slug'] = Str::slug($data['title']);

        $post->fill($data)->save();

        Alert::success('Статья успешно обновлена');

        return redirect()->route('platform.posts.edit', $post);
    }

    /**
     * @param Post $post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Post $post, Request $request)
    {
        $post->delete();

        Alert::success('Удалено');

        return redirect()->route('platform.posts');
    }
}
