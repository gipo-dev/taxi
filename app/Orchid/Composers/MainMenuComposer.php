<?php

declare(strict_types=1);

namespace App\Orchid\Composers;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemMenu;
use Orchid\Platform\Menu;

class MainMenuComposer
{
    /**
     * @var Dashboard
     */
    private $dashboard;

    /**
     * MenuComposer constructor.
     *
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Registering the main menu items.
     */
    public function compose()
    {
        // Profile
        $this->dashboard->menu
            ->add(Menu::PROFILE,
                ItemMenu::label('Action')
                    ->icon('icon-compass')
                    ->badge(function () {
                        return 6;
                    })
            )
            ->add(Menu::PROFILE,
                ItemMenu::label('Another action')
                    ->icon('icon-heart')
            );

        //Main
        $this->dashboard->menu
            ->add(Menu::MAIN,
                ItemMenu::label('Преимущества')
                    ->title('Главная')
                    ->route('platform.features')
            )
            ->add(Menu::MAIN,
                ItemMenu::label('Вопросы-ответы')
                    ->route('platform.questions')
            )
            ->add(Menu::MAIN,
                ItemMenu::label('Города')
                    ->route('platform.cities')
            )
            ->add(Menu::MAIN,
                ItemMenu::label('Статьи')
                    ->title('Статьи')
                    ->route('platform.posts')
            )
            ->add(Menu::MAIN,
                ItemMenu::label('Запросы')
                    ->title('Запросы')
                    ->route('platform.requests')
            );
    }
}
