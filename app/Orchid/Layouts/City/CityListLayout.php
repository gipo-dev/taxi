<?php

namespace App\Orchid\Layouts\City;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CityListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'cities';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('image', 'Изображение')
                ->align('center')
                ->render(function ($city) {
                    return "<img src='$city->image'>";
                }),
            TD::set('name', 'Название')->sort()->render(function ($city) {
                return Link::make($city->name)
                    ->route('city', $city->slug)
                    ->target('blank');
            }),
            TD::set('name_gen', 'Название род.'),
            TD::set('name_long', 'Полное название'),
            TD::set('id', '')
                ->align('center')
                ->render(function ($city) {
                    return Link::make('Ред')
                        ->route('platform.cities.edit', $city)
                        ->icon('icon-pencil')
                        ->class('btn btn-primary p-2');
                }),
        ];
    }
}
