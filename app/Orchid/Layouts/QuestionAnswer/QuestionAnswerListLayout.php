<?php

namespace App\Orchid\Layouts\QuestionAnswer;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class QuestionAnswerListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'questions';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('question', 'Вопрос')->width(300)->sort(),
            TD::set('answer', 'Ответ')->width(300)->sort(),
            TD::set('id', '')
                ->align('center')
                ->render(function ($question) {
                    return Link::make('Ред')
                        ->route('platform.questions.edit', $question)
                        ->icon('icon-pencil')
                        ->class('btn btn-primary p-2');
                }),
        ];
    }
}
