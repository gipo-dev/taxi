<?php

namespace App\Orchid\Layouts\Post;

use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class PostListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'posts';


    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID')
                ->align('center')
                ->sort()
                ->filter(TD::FILTER_TEXT)
                ->render(function ($post) {
                    return Link::make('Ред')
                        ->route('platform.posts.edit', $post)
                        ->icon('icon-pencil')
                        ->class('btn btn-primary p-2');
                }),
            TD::set('image', 'Изображение')
                ->align('center')
                ->render(function ($post) {
                    return "<img src='$post->image'>";
                }),
            TD::set('title', 'Заголовок')
                ->sort()
                ->filter(TD::FILTER_TEXT)
                ->render(function ($post) {
                    return Link::make($post->title)
                        ->route('post', $post->slug)
                        ->target('blank');
                }),
            TD::set('body', 'Тело статьи')
                ->filter(TD::FILTER_TEXT)
                ->render(function ($post) {
                    return mb_substr($post->short_body, 0, 70) . '...';
                }),
            TD::set('created_at', 'Дата публикации')
                ->filter(TD::FILTER_TEXT)
                ->sort()
                ->render(function ($post) {
                    return $post->created_at->format('d.m.Y H:i');
                }),
        ];
    }
}
