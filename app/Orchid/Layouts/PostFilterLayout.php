<?php

namespace App\Orchid\Layouts;

use App\Orchid\Filters\PostFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class PostFilterLayout extends Selection
{
    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            PostFilter::class,
        ];
    }
}
