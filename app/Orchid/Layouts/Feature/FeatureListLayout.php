<?php

namespace App\Orchid\Layouts\Feature;

use App\Feature;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class FeatureListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'features';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('icon', 'Иконка')
                ->align('center')
                ->render(function ($feature) {
                    return "<img src='$feature->icon'>";
                }),
            TD::set('text', 'Текст')->sort(),
            TD::set('id', '')
                ->align('center')
                ->render(function ($feature) {
                    return Link::make('Ред')
                        ->route('platform.features.edit', $feature)
                        ->icon('icon-pencil')
                        ->class('btn btn-primary p-2');
                }),
        ];
    }
}
