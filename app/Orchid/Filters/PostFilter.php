<?php

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;

class PostFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = [
        'title',
    ];

    /**
     * @return string
     */
    public function name(): string
    {
        return 'Заголовок';
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->whereLike('title', $this->request->get('title'));
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Input::make('title')->placeholder('Заголовок'),
        ];
    }
}
