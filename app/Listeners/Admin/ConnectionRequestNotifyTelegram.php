<?php

namespace App\Listeners\Admin;

use App\Events\Request\ConnectionRequestSent;
use App\Services\TelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ConnectionRequestNotifyTelegram
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ConnectionRequestSent $event
     * @return void
     */
    public function handle(ConnectionRequestSent $event)
    {
        $connectionRequest = $event->connectionRequest;
        $message = urlencode(implode(PHP_EOL, [
            'Имя: ' . $connectionRequest->name,
            'Телефон: ' . $connectionRequest->phone,
            'Страница: ' . $connectionRequest->source,
            $connectionRequest->created_at->format('H:i d.m.y'),
        ]));
        (new TelegramService())->send($message);
    }
}
