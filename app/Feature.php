<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Orchid\Access\UserAccess;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * @property int id
 * @property string text
 * @property string icon
 */

class Feature extends Model
{
    use AsSource, UserAccess, AsSource, Filterable, Chartable;

    /**
     * @var string[]
     */
    protected $casts = [
        'text' => 'string',
        'icon' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'text',
        'icon',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'text',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'text',
    ];
}
