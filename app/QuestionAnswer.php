<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Orchid\Access\UserAccess;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * Class QuestionAnswer
 * @package App
 *
 * @property int id
 * @property string question
 * @property string answer
 */

class QuestionAnswer extends Model
{
    use AsSource, UserAccess, AsSource, Filterable, Chartable;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string[]
     */
    protected $casts = [
        'question' => 'string',
        'answer' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'question',
        'answer',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'question',
        'answer',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'question',
        'answer',
    ];
}
