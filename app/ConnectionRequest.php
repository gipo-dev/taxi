<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Orchid\Access\UserAccess;
use Orchid\Filters\Filterable;
use Orchid\Metrics\Chartable;
use Orchid\Screen\AsSource;

/**
 * Class ConnectionRequest
 * @package App
 * @property string name
 * @property string phone
 * @property string ip
 * @property string source
 */
class ConnectionRequest extends Model
{

    use AsSource, UserAccess, AsSource, Filterable, Chartable;

    /**
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'ip' => 'string',
        'source' => 'string',
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'phone',
        'ip',
        'source',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'name',
        'phone',
        'ip',
        'source',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'name',
        'phone',
        'ip',
        'source',
    ];
}
