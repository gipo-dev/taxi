$(function () {
    $('#form-request-header, #form-request-category, #form-request-modal').submit(function (e) {
        e.preventDefault();
        sendRequest($(this));
        thanks();
    });

    function sendRequest($form, callback = function () {}) {
        $.ajax({
            url: $form.attr('action'),
            method: $form.attr('method') ? $form.attr('method') : 'GET',
            data: $form.serialize(),
            success: callback,
        });
    }

    function thanks() {
        var $target = $('#modal_thx');
        window.modal($target);
    }
});
