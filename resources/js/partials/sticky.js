window.sticky = function ($el) {
    $(document).on('scroll', function () {
        if (window.scrollY > 400)
            $el.addClass('sticky');
        else //if(window.scrollY < 1)
            $el.removeClass('sticky');
    });
}

$(function () {
    let $header = $('#header');
    sticky($header);

    if (window.innerWidth < 1000) {
        stickCalcResult();
        slideMenu();
    }
});

function stickCalcResult() {
    let $calc = $('.block-calculator');
    if ($calc.length < 1)
        return;
    let btop = $calc.offset().top;
    let bbottom = $calc.offset().top + $calc.outerHeight();
    $(document).on('scroll', function () {
        var pos = window.scrollY + window.outerHeight / 2;
        if (pos > btop && pos < bbottom)
            $calc.addClass('sticky');
        else
            $calc.removeClass('sticky');
    });
}

function slideMenu() {
    $('.navbar-toggler').click(function () {
        $('#navbarNav').slideToggle(300);
    });
}
