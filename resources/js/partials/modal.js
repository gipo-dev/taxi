$(function () {
    $('body').on('click', '[modal]', function (e) {
        e.preventDefault();
        let $target = $('#' + $(this).attr('modal'));

        if ($target.length < 1)
            return;

        window.modal($target);
    })

    $('.modal-container:not(.modal-form)').click(function () {
        closeModal($(this));
    });

    $('.modal-container .close').click(function () {
        closeModal($(this).parents('.modal-container'));
    });

    function closeModal($modal) {
        $modal.addClass('out');
        $('body').removeClass('modal-active');
        setTimeout(function () {
            $('.modal-container').removeClass(['open', 'out']);
        }, 200);
    }
});

window.modal = function ($target) {
    $('.modal-container').removeClass(['open', 'out']);
    $target.addClass('open');
    $('body').addClass('modal-active');
};
