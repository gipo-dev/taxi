$(function () {
    function range(wrap) {

        var sheet = document.createElement('style'),
            $rangeInput = wrap.find('.range input'),
            wrapId = '#' + wrap.attr('id'),
            sectionWidth = 100 / $rangeInput.attr('max');
            prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

        document.body.appendChild(sheet);

        wrap.find('.range-labels li').width((100 / (parseInt($rangeInput.attr('max')) + 1)) + '%');

        var getTrackStyle = function (el) {
            var curVal = el.value,
                val = (curVal) * sectionWidth,
                style = '';

            // Set active label
            wrap.find('.range-labels li').removeClass('active selected');

            var curLabel = wrap.find('.range-labels').find('li:nth-child(' + (parseInt(curVal) + 1)  + ')');

            curLabel.addClass('active selected');
            // curLabel.prevAll().addClass('selected');

            // Change background gradient
            for (var i = 0; i < prefs.length; i++) {
                style += wrapId + ' .range {background: linear-gradient(to right, #FFDF39 0%, #FFDF39 ' + val + '%, #C4C4C4 ' + val + '%, #C4C4C4 100%)}';
                style += wrapId + ' .range input::-' + prefs[i] + '{background: linear-gradient(to right, #FFDF39 0%, #FFDF39 ' + val + '%, #C4C4C4 ' + val + '%, #C4C4C4 100%)}';
            }

            return style;
        }

        $rangeInput.on('input', function () {
            sheet.textContent = getTrackStyle(this);
        });

// Change input value on label click
        wrap.find('.range-labels li').on('click', function () {
            var index = $(this).index();

            $rangeInput.val(index).trigger('input').trigger('change');

        });

        $rangeInput.trigger('input');
    }

    range($('#range-hpd'));
    range($('#range-dpv'));
});
