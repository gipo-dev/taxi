$(function () {
    var $reviewsSlider = $('.features');
    if ($reviewsSlider.length) {
        $reviewsSlider.slick({
            accessibility: false,
            centerMode: true,
            slidesToShow: 5,
            arrows: false,
            focusOnSelect: true,
            swipeToSlide: true,

            responsive: [{
                breakpoint: 1200,
                settings: {
                    initialSlide: 2,
                    slidesToShow: 3
                }
            }, {
                breakpoint: 767,
                settings: {
                    initialSlide: 2,
                    slidesToShow: 1,
                }
            }]
        });
    }
});
