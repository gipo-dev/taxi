// <script src="" async></script>
$(function () {
    $(window).one('scroll', function () {
        $jivoscript = $('jivoscript');
        if ($jivoscript.length < 1)
            return;
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = $jivoscript.attr('src') ?? '';
        $("head").append(s);
    });
});
