$(function () {
    let $form = $('#form-calculator');
    let $result = $('#income-text');

    //Доход в час
    let incomePH = [541.1, 714.3, 891.6,];
    let rentPerDay = [1300, 2200, 3000];
    let hpd = [2, 4, 6, 8, 10];
    let dpw = [1, 2, 3, 4, 5, 6, 7];

    function recalculate() {
        let values = getCalculatorValues();
        let result = hpd[values.hpd] * dpw[values.dpw] * 4 * incomePH[values.tariff];

        if (values.auto == 'rent') {
            result -= rentPerDay[values.tariff] * dpw[values.dpw] * 4;
        }

        $result.text(parseInt(result).toLocaleString('ru'));
    }

    function getCalculatorValues() {
        let result = {};
        $.each($form.serializeArray(), function () {
            result[this.name] = this.value;
        });
        return result;
    }

    recalculate();

    $form.find('input').on('input', function () {
        recalculate();
    });
});
