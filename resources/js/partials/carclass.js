$(function () {
    $block = $('#car-tariff');
    $brand_select = $('#car_brand');
    $model_select = $('#car_model');
    $result_block = $('#pg-ct-calc-res');

    if ($block.length < 1)
        return;

    carclass();

    $('#car-tariff').css({opacity: 1});

    $('.table-wrap .open').click(function () {
        $(this).parents('.table-wrap').addClass('opened');
    });



    function carclass() {
        $('.select2').select2();

        $brand_select.change(function () {
            let brand_id = $(this).val();
            $.ajax({
                url: '/tariffs/models',
                data: {
                    brand_id: brand_id,
                },
                success: function (resp) {
                    $model_select.empty().append(resp);
                    $model_select.select2("destroy");
                    $model_select.removeClass('disabled').select2();
                },
            });
        });

        $model_select.change(function () {
            let model_id = $(this).val();
            $.ajax({
                url: '/tariffs/car',
                data: {
                    model_id: model_id,
                },
                success: function (resp) {
                    $result_block.empty().append(resp);
                },
            });
        });
    }
});
