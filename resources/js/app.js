require('./bootstrap');
require('./partials/helpers');
require('./partials/sticky');
require('./partials/range');
require('./partials/droplist');
require('./partials/modal');
require('./partials/calculator');
require('./partials/request');
require('./partials/carclass');

require('./partials/scrollload');

require('./partials/slider');
