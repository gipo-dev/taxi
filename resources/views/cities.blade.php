@extends('partials.body')

@section('content')
    <div class="container page-cities-title">
        <h1>{{ __('page.header.links.cities') }}</h1>
    </div>
    <div class="container-fluid page-cities">
        <div class="row">
            @foreach($cities as $city)
                @include('partials.city', $city)
            @endforeach
        </div>
    </div>

    @include('partials.connect_block', ['page_name' => 'Подлючаемые города'])
@endsection
