@extends('partials.body')

@section('content')
    <section class="container-fluid">
        <div class="container py-5">
            <h1 class="mb-25">{{ __('page.posts.title') }}</h1>
            <div class="row">
                @foreach($posts as $post)
                    @include('partials.post', compact('post'))
                @endforeach
            </div>
            {{ $posts->links() }}
        </div>
    </section>
@endsection
