@extends('partials.body')

@section('content')
    <section class="container-fluid bg-dark block-slide post-full-image"
             style="background: url('{{ $post->image }}')">
        <div class="container">
            <div class="row">
                <div class="col-md-5 py-5 bg-split">
                    @include('partials.bread', ['crumbs' => [
                        'Главная' => '/',
                        'Статьи' => route('posts.list'),
                    ]])
                    <h1>{{ $post->title }}</h1>
                    <p class="date">Опубликовано: {{ $post->created_at->format('d.m.Y H:i') }}</p>
                </div>
                <div class="col-md-7 bg-img bg-img-header"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid post-full">
        <div class="container">
            @if(\Auth::user())
                <div style="padding-top: 15px;margin-bottom: -50px;">
                    <a href="{{ route('platform.posts.edit', $post->id) }}" class="btn btn-primary">Редактировать</a>
                </div>
            @endif
            <div class="py-5">
                <div class="post-body mb-25">
                    {!! $post->body !!}
                </div>
            </div>
        </div>
    </section>

    @include('partials.connect_block', ['page_name' => 'Статья - ' . $post->title ?? 'Санкт-Петербург'])

    <section class="container py-5">
        <h2 class="mb-25">{{ __('page.posts.related') }}</h2>
        <div class="row">
            @foreach($post->related() as $related)
                <div class="col-md-4">
                    @include('partials.post_related', ['post' => $related])
                </div>
            @endforeach
        </div>
    </section>
@endsection
