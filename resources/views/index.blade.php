@extends('partials.body')

@section('content')
    <section class="container-fluid bg-dark block-slide">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-5 py-5 bg-split">
                    <h1>{!! __('page.index.slide.h1', ['city' => $city->name_gen ?? 'Санкт-Петербурге']) !!}</h1>
                    <p>{!! __('page.index.slide.text', ['city' => $city->name_long ?? 'Санкт-Петербурге и Ленинградской области']) !!}</p>
                    <form action="{{ route('request.send') }}" method="POST" id="form-request-header">
                        @csrf
                        <input type="hidden" name="name" value="Неизвестно">
                        <input type="hidden" name="source" value="Шапка {{ $city->name ?? 'Санкт-Петербург' }}">
                        <input type="hidden" name="ip" value="{{ request()->ip() ?? '' }}">
                        <input type="text" name="phone" placeholder="{{ __('page.index.slide.input') }}"
                               minlength="5" required>
                        <button type="submit" class="btn btn-primary">
                            {{ __('page.index.slide.button') }}
                        </button>
                    </form>
                </div>
                <div class="col-md-7 bg-img bg-img-header d-none d-lg-block"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid block-crown">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-5 py-5 bg-split">
                    <h2>{!! __('page.index.crown.title') !!}</h2>
                    <ul>
                        @foreach(__('page.index.crown.features') as $f)
                            <li>{!! $f !!}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-7 bg-img bg-img-header d-none d-lg-block"></div>
            </div>
        </div>
    </section>

    <section class="py-5 block-features">
        {{--        <h2>{{ __('page.index.features.title') }}</h2>--}}
        <div class="features">
            @foreach($features as $feature)
                <div class="feature">
                    <span class="icon">
                        @include('partials.icons.features.'.$feature->id)
                    </span>
                    <p>{!! $feature->text !!}</p>
                </div>
            @endforeach
        </div>
    </section>

    <section class="container-fluid block-crown block-rent">
        <div class="container">
            <div class="row">
                <div class="col-md-7 bg-img bg-img-header d-none d-lg-block"></div>
                <div class="col-md-12 col-lg-5 py-5 bg-split">
                    <h2>{!! __('page.index.rent.title') !!}</h2>
                    <ul>
                        @foreach(__('page.index.rent.features') as $f)
                            <li>{!! $f !!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid block-gas">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-5 py-5 bg-split">
                    <h2>{!! __('page.index.gas.title') !!}</h2>
                    <p>{!! __('page.index.gas.sentence_1', ['city' => $city->name_long ?? 'Санкт-Петербурге и Ленинградской области']) !!}</p>
                    <p>{!! __('page.index.gas.sentence_2', ['city' => $city->name_long ?? 'Санкт-Петербурге и Ленинградской области']) !!}</p>
                </div>
                <div class="col-md-7 bg-img bg-img-header d-none d-lg-block"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid bg-gray py-5 block-calculator">
        <div class="container">
            <h2>{{ __('page.index.calculator.title') }}</h2>
            <p>{!! __('page.index.calculator.description') !!}</p>
            <div class="row">
                <div class="col-12 col-lg-5">
                    <form action="" id="form-calculator">
                        <div class="range-wrap" id="range-hpd">
                            <label>{{ __('page.index.calculator.range_1') }}</label>
                            @include('partials.range', [ 'items' => [ 2, 4, 6, 8, 10, ], 'value' => 3, 'name' => 'hpd' ])
                        </div>
                        <div class="range-wrap fl" id="range-dpv">
                            <label>{{ __('page.index.calculator.range_2') }}</label>
                            @include('partials.range', [ 'items' => [ 1, 2, 3, 4, 5, 6, 7, ], 'value' => 4, 'name' => 'dpw' ])
                        </div>
                        <div class="mb-25">
                            <label class="mb-15">{{ __('page.index.calculator.question_1') }}</label>
                            <div>
                                <label class="radio">
                                    <input type="radio" name="auto" value="own" checked>
                                    <span>{{ __('page.index.calculator.car_own') }}</span>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="auto" value="rent">
                                    <span>{{ __('page.index.calculator.car_rent') }}</span>
                                </label>
                            </div>
                        </div>
                        <div class="mb-25">
                            <label class="mb-15">{{ __('page.index.calculator.question_2') }}</label>
                            <div>
                                <label class="radio">
                                    <input type="radio" name="tariff" value="0" checked>
                                    <span>{{ __('page.index.calculator.tariff_1') }}</span>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="tariff" value="1">
                                    <span>{{ __('page.index.calculator.tariff_2') }}</span>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="tariff" value="2">
                                    <span>{{ __('page.index.calculator.tariff_3') }}</span>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-7 block-income">
                    <div>
                        <p>{{ __('page.index.calculator.income') }}</p>
                        <span class="income"><b id="income-text">62 500</b>₽</span>
                        <p class="comment mb-25">{!! __('page.index.calculator.income_comment') !!}</p>
                        <a href="#" class="btn btn-primary"
                           modal="modal_request">{{ __('page.header.left_request') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container block-questions py-5">
        <h2>{{ __('page.index.questions.title') }}</h2>

        <ul class="droplist">
            @foreach($questions as $qa)
                <li>
                    <span>{!! $qa->question !!} <svg width="32" height="32" viewBox="0 0 32 32" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg" class="_2F_jc"><path
                                d="M13 9l7 7-7 7" stroke="rgb(153, 153, 153)" stroke-width="1.8"
                                stroke-linecap="round"></path></svg></span>
                    <p>{!! $qa->answer !!}</p>
                </li>
            @endforeach
        </ul>
    </section>
@endsection
