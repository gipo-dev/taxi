<div class="col-md-4 city">
    <a href="{{ route('city', $city) }}">
        <img src="{{ $city->image }}" alt="{{ $city->name }}">
        <h2>{{ $city->name }}</h2>
    </a>
</div>
