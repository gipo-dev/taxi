<ul class="range-labels">
    @foreach($items as $i => $item)
        <li class="{{ $i == ($value ?? 0) ? 'active selected' : '' }}">{{ $item }}</li>
    @endforeach
</ul>
<div class="range">
    <input type="range" min="0" max="{{ count($items) - 1 ?? 10 }}" steps="1" value="{{ $value ?? 0 }}" name="{{ $name ?? '' }}">
</div>
