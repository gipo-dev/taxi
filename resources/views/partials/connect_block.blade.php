<section class="bg-primary">
    <div class="container py-5 block-connect">
        <p class="h2">{{ __('page.loops.connect.title') }}</p>
        <p class="description">{{ __('page.loops.connect.description') }}</p>
        <form action="{{ route('request.send') }}" method="POST" id="form-request-category">
            @csrf
            <input type="hidden" name="source" value="{{ $page_name ?? '' }}">
            <input type="hidden" name="ip" value="{{ request()->ip() ?? '' }}">
            <input type="text" name="name" placeholder="{{ __('page.loops.forms.name') }}" required>
            <input type="text" name="phone" placeholder="{{ __('page.loops.forms.phone') }}" required>
            <button type="submit" class="btn btn-dark">{{ __('page.header.left_request') }}</button>
        </form>
    </div>
</section>
