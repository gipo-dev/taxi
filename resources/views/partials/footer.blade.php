<footer class="footer bg-dark py-5">
    <div class="container">
        <div class="row mb-25">
            <ul class="list-unstyled col-6 col-md-4">
                <li><a href="#">{{ __('page.header.links.auto') }}</a></li>
                <li><a href="#">{{ __('page.header.links.cities') }}</a></li>
                <li><a href="#">{{ __('page.header.links.articles') }}</a></li>
            </ul>
            <ul class="list-unstyled col-6 col-md-3">
                <li><a href="/#">{{ __('page.index.calculator.title') }}</a></li>
                <li><a href="/#">{{ __('page.index.questions.title') }}</a></li>
                <li><a href="#">{{ __('page.header.left_request') }}</a></li>
            </ul>
            <div class="col-md-5 offset-lg-2 col-lg-3 numbers">
                <div class="row">
                    <a href="tel:{{ __('page.header.phone_1_call') }}" class="col-6 col-md-12">{{ __('page.header.phone_1') }}</a>
                    <a href="tel:{{ __('page.header.phone_2_call') }}" class="col-6 col-md-12">{{ __('page.header.phone_2') }}</a>
                </div>
            </div>
        </div>
        <p class="copyright">{{ __('page.index.footer.copyright') }}</p>
    </div>
</footer>

@push('modals')
    <div class="modal-container" id="modal_thx">
        <div class="modal-background">
            <div class="modal">
                <p class="modal-title">{{ __('page.modal.thanks.title') }}</p>
                <p class="modal-comment">{{ __('page.modal.thanks.comment') }}</p>
                <svg width="150" height="150" viewBox="0 0 510 510" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink">
                    <path
                        d="M150.45,206.55l-35.7,35.7L229.5,357l255-255l-35.7-35.7L229.5,285.6L150.45,206.55z M459,255c0,112.2-91.8,204-204,204 S51,367.2,51,255S142.8,51,255,51c20.4,0,38.25,2.55,56.1,7.65l40.801-40.8C321.3,7.65,288.15,0,255,0C114.75,0,0,114.75,0,255 s114.75,255,255,255s255-114.75,255-255H459z"></path>
                </svg>
            </div>
        </div>
    </div>
@endpush

@push('modals')
    <div class="modal-container modal-form" id="modal_request">
        <div class="modal-background">
            <div class="modal">
                <span class="close"></span>
                <p class="modal-title">{{ __('page.header.left_request') }}</p>
                <form action="{{ route('request.send') }}" method="POST" id="form-request-modal">
                    @csrf
                    <input type="hidden" name="source" value="Модальное окно: {{ request()->url() }}">
                    <input type="hidden" name="ip" value="{{ request()->ip() ?? '' }}">
                    <input type="text" name="name" placeholder="{{ __('page.loops.forms.name') }}" required>
                    <input type="text" name="phone" placeholder="{{ __('page.loops.forms.phone') }}" required>
                    <button type="submit" class="btn btn-primary">{{ __('page.header.left_request') }}</button>
                </form>
            </div>
        </div>
    </div>
@endpush
