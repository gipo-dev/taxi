<a href="{{ route('post', $post) }}" class="card mb-25 post-small">
    <div class="col-12 col-sm-4 post-image">
        <img src="{{ $post->image }}" alt="{{ $post->title }}">
    </div>
    <div class="col-12 col-sm-8 post-content">
        <h3 class="post-title">{{ $post->title }}</h3>
        <p class="post-body">{!! $post->short_body !!}...</p>
        <span class="read-more">{{ __('page.posts.read_more') }}</span>
    </div>
</a>
