<header class="container-fluid bg-dark" id="header">
    <nav class="container navbar navbar-expand-lg navbar-light">
        <div class="row">
            <a class="navbar-brand" href="/">{{ __('page.header.logo') }}</a>
            <button class="navbar-toggler d-inline-block d-md-none" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                @include('partials.icons.burger')
            </button>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link phone" href="tel:{{ __('page.header.phone_1_call') }}">{{ __('page.header.phone_1') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link phone hd" href="tel:{{ __('page.header.phone_2_call') }}">{{ __('page.header.phone_2') }}</a>
                    </li>
                </ul>
            </div>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('tariffs') }}">{{ __('page.header.links.auto') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('cities.list') }}">{{ __('page.header.links.cities') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('posts.list') }}">{{ __('page.header.links.articles') }}</a>
                    </li>
                </ul>
            </div>
            <a href="#" class="btn btn-primary" modal="modal_request">{{ __('page.index.slide.button') }}</a>
        </div>
    </nav>
</header>
