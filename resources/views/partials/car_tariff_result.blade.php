<h2>{{ __('page.tariff.calculator.result.title', compact('car')) }}</h2>
@foreach($tariffs as $tariff)
    <p>{!! __('page.tariff.calculator.result.tariff', ['tariff' => $tariff->name]) !!} {{ $tariff->tariff_requirements }}</p>
@endforeach
<a href="#" class="btn btn-dark" modal="modal_request">{{ __('page.header.left_request_connect') }}</a>
