<div class="breadcrumbs">
    @foreach(collect($crumbs) as $text => $link)
        <a href="{{ $link }}">{{ $text }}</a>
    @endforeach
</div>
