<a href="{{ route('post', $post) }}" class="card mb-25 post-related">
    <img src="{{ $post->image }}" alt="{{ $post->title }}">
    <div class="post-content">
        <p class="post-title">{{ $post->title }}</p>
        <span class="read-more">{{ __('page.posts.read_more') }}</span>
    </div>
</a>
