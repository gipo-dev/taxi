@extends('partials.body')

@section('content')
    <div class="container-fluid bg-primary py-5 page-car-tariff page-car-tariff-calculator">
        <div class="container">
            <h1>{{ __('page.tariff.title') }}</h1>
            <form id="car-tariff">
                <select name="brand" id="car_brand" class="select2">
                    <option value="-1" selected disabled>Выберите марку</option>
                    @foreach(\App\Cars\CarBrand::get() as $brand)
                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                    @endforeach
                </select>
                <select name="model" id="car_model" class="select2 disabled">
                    <option value="">Выберите модель</option>
                </select>
            </form>
            <div class="result" id="pg-ct-calc-res">
            </div>
        </div>
    </div>
    <div class="container page-car-tariff">
        <div class="row">
            <div class="col-12">
                @foreach($tariffs as $tariff)
                    <h2>{{ __('page.tariff.table.title', ['class' => $tariff->name]) }}</h2>
                    <div class="table-wrap">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <td>Марка и модель</td>
                                <td>Год допуска</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tariff->cars as $car)
                                <tr>
                                    <td>{{ $car->model->brand->name ?? '' }} {{ $car->model->name ?? '' }}</td>
                                    <td>{{ $car->tariff_requirements }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="open">
                            <span class="btn btn-primary">Показать полностью</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @include('partials.connect_block', ['page_name' => 'К какому тарифу подходит авто'])
@endsection

@push('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endpush
