<?php

return [
    'default' => [
        'title' => 'Работа в Яндекс Такси в Санкт-Петербурге',
        'description' => 'Работа в Яндекс Такси в Санкт-Петербурге и Ленинградской области. Низкая комиссия. Помощь иностранцам в подключении.',
    ],

    'index' => [
        'title' => 'Работа в Яндекс Такси в Санкт-Петербурге',
        'description' => 'Работа в Яндекс Такси в Санкт-Петербурге и Ленинградской области. Низкая комиссия. Помощь иностранцам в подключении.',
    ],

    'city' => [
        'title' => 'Работа в Яндекс Такси в ',
        'description' => 'Работа в Яндекс Такси в :city. Низкая комиссия. Помощь иностранцам в подключении.',
    ],

    'tariffs' => [
        'title' => 'К какому тарифу Яндекс Такси подходит автомобиль',
        'description' => 'Список автомобилей, которые подходят для тарифов Яндекс Такси Эконом, Комфорт, Минивэн, Детский, Ultima',
    ],

    'cities' => [
        'list' => [
            'title' => 'Подключаемые города',
            'description' => 'Список городов, в которых вы можете подключиться к таксопарку Яндекс Такси',
        ],
    ],

    'posts' => [
        'list' => [
            'title' => 'Статьи и новости для таксистов',
            'description' => 'Статьи и новости для водителей Яндекс Такси, которые хотят быть в курсе всех новинок в сфере такси',
        ],
    ],

];
