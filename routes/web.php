<?php

use Illuminate\Support\Facades\Route;
use Orchid\Platform\Http\Controllers\Systems\IndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');

//Posts
Route::get('posts', 'PostController@list')->name('posts.list');
//Route::get('posts/categories/{category}', 'PostController@category')->name('posts.category');
Route::get('posts/{post}', 'PostController@index')->name('post');

//Request
Route::post('request/send', 'RequestController@send')->name('request.send');

//Tariffs
Route::get('tariffs', 'TariffController@index')->name('tariffs');
Route::get('tariffs/models', 'TariffController@models')->name('tariffs.models');
Route::get('tariffs/car', 'TariffController@car')->name('tariffs.car');

//Services
Route::get('service/tariff', 'Services\\CarTariff@handle');
Route::get('dashboard', [IndexController::class, 'index']);


//Cities
Route::get('goroda', 'CityController@list')->name('cities.list');
Route::get('{city}', 'CityController@index')->name('city');
