<?php

declare(strict_types=1);

use App\Orchid\Screens\City\CityEditScreen;
use App\Orchid\Screens\City\CityListScreen;
use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\Feature\FeatureEditScreen;
use App\Orchid\Screens\Feature\FeatureListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Post\PostEditScreen;
use App\Orchid\Screens\Post\PostListScreen;
use App\Orchid\Screens\QuestionAnswer\QuestionAnswerEditScreen;
use App\Orchid\Screens\QuestionAnswer\QuestionAnswerListScreen;
use App\Orchid\Screens\Request\RequestListScreen;
use App\Orchid\Screens\Request\RequestViewScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)->name('platform.main');

// Users...
Route::screen('users/{users}/edit', UserEditScreen::class)->name('platform.systems.users.edit');
Route::screen('users', UserListScreen::class)->name('platform.systems.users');

// Roles...
Route::screen('roles/{roles}/edit', RoleEditScreen::class)->name('platform.systems.roles.edit');
Route::screen('roles/create', RoleEditScreen::class)->name('platform.systems.roles.create');
Route::screen('roles', RoleListScreen::class)->name('platform.systems.roles');

// Features...
Route::screen('features/list', FeatureListScreen::class)->name('platform.features');
Route::screen('features/{feature?}', FeatureEditScreen::class)->name('platform.features.edit');

// QuestionAnswer...
Route::screen('questions/list', QuestionAnswerListScreen::class)->name('platform.questions');
Route::screen('questions/{question?}', QuestionAnswerEditScreen::class)->name('platform.questions.edit');

// Cities...
Route::screen('cities/list', CityListScreen::class)->name('platform.cities');
Route::screen('cities/{city?}', CityEditScreen::class)->name('platform.cities.edit');

// Posts...
Route::screen('posts/list', PostListScreen::class)->name('platform.posts');
Route::screen('posts/{city?}', PostEditScreen::class)->name('platform.posts.edit');

// Requests...
Route::screen('requests/list', RequestListScreen::class)->name('platform.requests');
Route::screen('requests/{cr?}', RequestViewScreen::class)->name('platform.requests.view');

