<?php

declare(strict_types=1);

use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

//Screens

// Platform > System > Users
Breadcrumbs::for('platform.systems.users', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.systems.index');
    $trail->push(__('Users'), route('platform.systems.users'));
});

// Platform > System > Users > User
Breadcrumbs::for('platform.systems.users.edit', function (BreadcrumbsGenerator $trail, $user) {
    $trail->parent('platform.systems.users');
    $trail->push(__('Edit'), route('platform.systems.users.edit', $user));
});

// Platform > System > Roles
Breadcrumbs::for('platform.systems.roles', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.systems.index');
    $trail->push(__('Roles'), route('platform.systems.roles'));
});

// Platform > System > Roles > Create
Breadcrumbs::for('platform.systems.roles.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.systems.roles');
    $trail->push(__('Create'), route('platform.systems.roles.create'));
});

// Platform > System > Roles > Role
Breadcrumbs::for('platform.systems.roles.edit', function (BreadcrumbsGenerator $trail, $role) {
    $trail->parent('platform.systems.roles');
    $trail->push(__('Role'), route('platform.systems.roles.edit', $role));
});

// Platform -> Example Screen
Breadcrumbs::for('platform.example', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Example screen'));
});

// Platform -> Example Fields
Breadcrumbs::for('platform.example.fields', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Form controls'));
});

// Platform -> Example Layouts
Breadcrumbs::for('platform.example.layouts', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Overview layouts'));
});

// Platform -> Features
Breadcrumbs::for('platform.features', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push('Преимущества', route('platform.features'));
});

Breadcrumbs::for('platform.features.edit', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.features');
    $trail->push('Преимущество');
});

// Platform -> Questions
Breadcrumbs::for('platform.questions', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push('Вопросы', route('platform.questions'));
});

Breadcrumbs::for('platform.questions.edit', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.questions');
    $trail->push('Вопрос');
});

// Platform -> Cities
Breadcrumbs::for('platform.cities', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push('Города', route('platform.cities'));
});

Breadcrumbs::for('platform.cities.edit', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.cities');
    $trail->push('Город');
});

// Platform -> Posts
Breadcrumbs::for('platform.posts', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push('Статьи', route('platform.posts'));
});

Breadcrumbs::for('platform.posts.edit', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.posts');
    $trail->push('Статья');
});


// Platform -> Requests
Breadcrumbs::for('platform.requests', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.index');
    $trail->push('Запросы', route('platform.requests'));
});

Breadcrumbs::for('platform.requests.view', function (BreadcrumbsGenerator $trail) {
    $trail->parent('platform.requests');
    $trail->push('Запрос');
});
